function  PlotAll(GAM1,Lvec1,Dvec1,Tvec1,Vvec,Timvec,CLvec,W)
PlotForces(GAM1,Lvec1,Dvec1,Tvec1,CLvec,W)
figure

plot(Timvec,GAM1,'linewidth',1.5);
hold on;
ylabel('$\gamma$ (rad)','interpreter','latex')
xlabel('Time (s)','interpreter','latex')
title("$\gamma$ vs time",'interpreter','latex')
grid on;

figure

plot(GAM1,Vvec,'linewidth',1.5);
hold on;

xlabel('$\gamma$ (rad)','interpreter','latex')
ylabel('Speed (m/s)','interpreter','latex')
title("V vs $\gamma$",'interpreter','latex')
grid on;
end

