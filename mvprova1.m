close all 
clear

N= 100;
R= 4;

theta= linspace(0,7*pi,N);
r= zeros(N,3);


for i=1:N
    
   
    
    if theta(i) <= pi/2
        r(i,1) = R*sin(theta(i));
        r(i,2) = R*(1-cos(theta(i)));
        r(i,3) = R;
        
    
    elseif theta(i) > pi/2 && theta(i) <= pi
        
        r(i,1) = R*sin(pi/2);
        r(i,2)= R*(1-cos(theta(i)));
        r(i,3) = R*cos(theta(i)-pi/2);
        
        
        
    elseif theta(i) > pi && theta(i) <= 5*pi/2
        r(i,1) = R;
        r(i,2)= R*(1-cos(theta(i)));
        r(i,3) = R*sin(theta(i));
        
    elseif theta(i) > pi*5/2 && theta(i)<= pi/2*5+2*pi
        
        r(i,1) = 2*R-R*cos(theta(i)-5*pi/2);
        r(i,2)= R+R*sin(theta(i)-5*pi/2);
        r(i,3) = R;
%         r(i,1) = 2*R*(sin(-(theta(i)-pi/2)));
%         r(i,2) = R+R*cos(theta(i)-pi/2);
%         r(i,3) = R;
%         
    elseif theta(i) > 9*pi/2 && theta(i)<= 9*pi/2+2*pi
        r(i,1) = R;
        r(i,2)= R+R*sin(theta(i)-9*pi/2);
        r(i,3) = 2*R-R*cos(theta(i)-9*pi/2);
        
    elseif theta(i) > 13*pi/2 && theta(i)<= 13*pi/2+2*pi
        r(i,1) = R*cos(theta(i)-13*pi/2);
        r(i,2)= R+R*sin(theta(i)-13*pi/2);
        r(i,3) = R;
    
    end
end



plot3(r(:,3),r(:,1),r(:,2),'r')
