%clear all;
close all;
N = 100;
%% Init
%data double
cd0 = 0.02;
k = 0.1;
S =11.25;
g = 9.81;
R = 150;%75.07;
rho = 1.225;
P = 235e3;
m = 850;
delta = 1e-7;
V0 = 81.07; %velocitat inicial
%memory allocation

time = linspace(0,2.7,N);
deltaT = time(2)-time(1);
Gammavec = zeros(1,N);
Lvec = Gammavec;
Vvec = Gammavec;
Dvec = Gammavec;
CLvec = Gammavec;


%% Initial iteration
gamman = 3*pi/2;%3*pi/2; % gamma inicial cal canviar-ho a la linia 39 tmb
gammanm = gamman- V0*deltaT/R;
[CL, gamma,V] = iterate(cd0,k,deltaT,gamman,gammanm,S,g,R,rho,m,P,delta)
CLvec(1)=CL;
Gammavec(1)=gamma;
Vvec(1)=V;
Lvec(1)=0.5*rho*V^2*CL*S;
Dvec(1)=0.5*rho*V^2*S*(cd0+k*CL^2);
%% Iteration
for i = 2:N
    if (i ==2)
    gammanm= 3*pi/2;
    else
    gammanm=Gammavec(i-2);
    end
    gamman = Gammavec(i-1);
    [CL, gamma,V] = iterate(cd0,k,deltaT,gamman,gammanm,S,g,R,rho,m,P,delta)
    CLvec(i)=CL;
    Gammavec(i)=gamma;
    Vvec(i)=V;
    Lvec(i)=0.5*rho*V^2*CL*S;
    Dvec(i)=0.5*rho*V^2*S*(cd0+k*CL^2);
    
end
PlotAll(Gammavec,Lvec,Dvec,P./Vvec,Vvec,time,CLvec,m*g)
