%%%%% Cloverleaf analysis for XtremeAir XA41 %%%%%%%%
close all;
clear all;
%% Aircraft input data        https://www.xtremeair.com/xa41/

b = 7.5;
S = 11.25;
m = 850;
P = 235e3;
vstall = 94/3.6;
rollrated = 450; %degrees/sec 
etaf = 1;
vcruise = 345/3.6;


%% Atmosphere input data
g = 9.81;
rho = 1.225;

%% Aerodynamic data

AR = b^2/S;
W = m*g;
CLmax = 2*W/(rho*vstall^2*S);
rollrate = rollrated*2*pi/360;

qinf = 1/2 * rho * vcruise^2;
qinfS = 1/2 * rho * vcruise^2 * S;
CLcruise = W/(qinfS)
T = P/vcruise





%% Simple Loop
N = 100;
x = zeros(1,N);
Y = zeros(1,N);
z = zeros(1,N);
t = linspace (0,5,N);
 
Rmin = (vcruise^2) / (qinfS*CLmax / m - g);

gampunt = vcruise/Rmin;

plloop = animatedline;
axis([-80 80 -1 1 -5 155])
view(3);
for k = 1:length(t)
    
   y = 0;
   x = Rmin * sin(gampunt*t(k));
   z = Rmin *(1-cos(gampunt*t(k)));
   plloop.LineWidth=2;
   addpoints(plloop,x,y,z)
   drawnow
   if (k==N)
       pause(t(k)-t(k-1));
   else
       pause(t(k+1)-t(k));
   end

    
end