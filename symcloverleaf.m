clear all;
close all;
tic

%% Despres dels comentaris 
% syms CL0 CLalpha CD0 k S m
% 
% syms g rho
% 
% syms t
% 
% syms gamma(t) mu alpha(t) epsilon nu P
% 
% syms R
% 
% V = diff(gamma(t)) * R
% 
% CL = CL0 + alpha * CLalpha;
% L = 0.5 * rho * S * V^2 * CL;
% CD = CD0 + k* CL^2;
% D = 0.5 * rho * S * V^2 * CD;
% 
% %% MOVIMENT CIRCULAR
% 
% an = diff(gamma(t),t,2)*R;
% at = diff(gamma(t),t)^2 * R;
% 
% syms T ;%= P/V;
% 
% atfgen = -m* g* sin(gamma) + T*cos(epsilon)* cos(nu) - D;
% anfgen = m*g * cos(gamma) * cos(mu) - T * sin(epsilon) - L;
% 
% atf = subs (atfgen,[epsilon nu mu],[0 0 0]);
% anf = subs (anfgen,[epsilon mu],[0 0]);
% 
% 
% eqt = atf == at;
% eqn = anf == an;
% 
% syms Rmin Vini alpha0
% 
% 
% CD0_v = 0.05;
% k_v = 0.03;
% CLalpha_v = 5.5;
% CL0_v = 0;
% P_v = 235e3;
% g_v = 9.81;
% rho_v = 1.225;
% m_v = 850;
% V_ini = 345/3.6
% CL_max = 1.1;
% S_v = 11.25;
% 
% R_v = V_ini^2 /(.5*rho_v *V_ini^2*S_v*CL_max/m_v -g_v);
% 
% 
% 
% param =[CD0_v k_v CLalpha_v CL0_v P_v/V_ini g_v rho_v m_v S_v R_v];
% symparam = [CD0 k CLalpha CL0 T g rho m S R];
% 
% eqnv = subs(eqn,symparam,param);
% eqtv = subs(eqt,symparam,param);
% 
% condini1 = gamma(0) == 0;
% condini2 = alpha(0) == (CL_max-CL0_v)/CLalpha_v;
% Dg = diff(gamma(t),t)
% condini3 = Dg == Vini/Rmin
% 
% S = dsolve([eqnv eqtv],[condini1 condini2 condini3])%, [condini1 condini2 condini3])
% 
% toc
%% V2
syms t
syms m g gamma(t) T(t) D(t) V(t) L(t) gamma(t) mu(t) chi(t) Rr Rl


eq1 = -m*g*sin(gamma) + T -D == m * diff(V,t);

eq2 = L * sin(mu) == m * diff(chi,t)^2*Rr;

eq3 = m*g*cos(gamma) -L *cos(mu) == -m*diff(gamma,t)^2*Rl;
pretty(eq1)
pretty(eq2)
pretty(eq3)