function [CL, gamma,V] = iterate(cd0,k,deltaT,gamman,gammanm,S,g,R,rho,m,P,delta)

% gamma is gamma(n+1)
% gamman is gamma(n)
% gammanm is gamma(n-1)
% 
    M = 10000;
    differ = zeros(M,1);
	gamma = gamman +(gamman-gammanm);
	for(i =1:M)
		gammapunt = (gamma-gamman)/deltaT;
		gammapuntpunt = (gamma-2*gamman+gammanm)/(deltaT^2);
		V = gammapunt*R;
        T = P/V;
        iter = i;
		qinfS = 0.5*rho*V^2*S;
		CL = (m*g*cos(gamma)+m*gammapunt^2*R)/qinfS;
		D = qinfS*(cd0+k*CL^2);
		differ(i,1) = T-D-m*g * sin(gamma)- m * gammapuntpunt*R;
		if (abs(differ/(m*g))<delta)
		
		break
        end
        %if(abs(differ))
		gamma = (1+(differ(i,1))/(1e9))*gamma;
	end
	
%plot(differ)
end