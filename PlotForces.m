function PlotForces(GAM1,Lvec1,Dvec1,Tvec1,CLvec,W)
figure

plot(GAM1,Dvec1,'linewidth',1.5);
hold on;
plot(GAM1,Tvec1,'linewidth',1.5,'linestyle','--');
plot(GAM1,-Dvec1+Tvec1,'linewidth',1.5,'linestyle','-.');
xlabel('$\gamma$ (rad)','interpreter','latex')
ylabel('Force (N)','interpreter','latex')
legend('D','T','T-D','interpreter','latex')
title("D and T vs $\gamma$",'interpreter','latex')
grid on;

figure
yyaxis left
plot(GAM1,Lvec1,'linewidth',1.5)
title("L vs $\gamma$",'interpreter','latex')
ylabel('L (N)','interpreter','latex')
yyaxis right
plot(GAM1,CLvec,'linewidth',1.5,'linestyle','--');
hold on
plot(GAM1,Lvec1/W,'linewidth',1.5,'linestyle','-.');
xlabel('$\gamma$ (rad)','interpreter','latex')
legend('L','$C_L$','n','interpreter','latex')
grid on;
end

