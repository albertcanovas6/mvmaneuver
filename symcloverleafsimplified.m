clear all 
close all

syms clmax k cdo gam m v rho s g

qinf = 0.5 * v^2 * rho;
qinfs = qinf*s;
r = v^2/(qinfs*clmax/m -g);

gampunt = v/r;

cl = m * (g * cos(gam) + v^2/r)/qinfs;

cd = cdo + k * cl ^2;

l = qinfs * cl;

d = qinfs * cd;

t = d + m * g * sin(gam);

%% Primer quart de loop

CLMAX = 1.774;
S = 11.25;
M = 850;
G = 9.81;
CD0 = 0.02;
K = 0.1;
RHO = 1.225;
V =  345/3.6;

symbs = [clmax k cdo m v rho s g];
vals = [CLMAX K CD0 M V RHO S G];
L = subs(l,symbs,vals);
D = subs(d,symbs,vals);
T = subs(t,symbs,vals);
CL = subs(cl,symbs,vals);

GAM1 = linspace(0,pi/2,100);

Lvec1 = subs(L,gam,GAM1);
Dvec1 = subs(D,gam,GAM1);
Tvec1 = subs(T,gam,GAM1);
CLvec1 = subs(CL,gam,GAM1);

PlotForces(GAM1,Lvec1,Dvec1,Tvec1,CLvec1,M*G);
%clear all;
%% Tram de roll
tic
rollrated = 450;
dtpuj = 90/rollrated;
syms t cdo m v(t) rho s g thr v1
eq1 = thr - 0.5 * rho * s * v^2 * cdo - m * g == m*diff(v,t);
cond1 = v(0) == v1;
vt = dsolve(eq1,cond1);
xt = int(vt,t,[0 t]);
S = 11.25;
M = 850;
G = 9.81;
CD0 = 0.02;
RHO = 1.225;
V1 = 60.29; 
THR = 235000/V1;
T = linspace(0,dtpuj,100);
symbs = [cdo m rho s g thr v1];
vals = [ CD0 M RHO S G THR V1];
VT = subs(vt,symbs,vals);
VTvec = subs(VT,t,T);

XT = subs(xt,symbs,vals);
XTvec = subs(XT,t,T);

figure
plot(T,VTvec,'linewidth',1.5)
xlabel("Time (s)",'interpreter','latex')
ylabel("Velocity (m/s)",'interpreter','latex')
title("Velocity vs time",'interpreter','latex')
figure
plot(T,XTvec,'linewidth',1.5)
xlabel("Time (s)",'interpreter','latex')
ylabel("Distance (m)",'interpreter','latex')
title("Distance covered",'interpreter','latex')
toc
%% 2n quart tram de loop
syms clmax k cdo gam m v rho s g

qinf = 0.5 * v^2 * rho;
qinfs = qinf*s;
r = v^2/(qinfs*clmax/m -g);

gampunt = v/r;

cl = m * (g * cos(gam) + v^2/r)/qinfs;

cd = cdo + k * cl ^2;

l = qinfs * cl;

d = qinfs * cd;

t = d + m * g * sin(gam);

CLMAX = 1.774;
S = 11.25;
M = 850;
G = 9.81;
CD0 = 0.02;
K = 0.1;
RHO = 1.225;
V = 94.16;

symbs = [clmax k cdo m v rho s g];
vals = [CLMAX K CD0 M V RHO S G];
L = subs(l,symbs,vals);
D = subs(d,symbs,vals);
T = subs(t,symbs,vals);
CL = subs(cl,symbs,vals);

GAM2 = linspace(pi/2,3*pi/2,100);

Lvec2 = subs(L,gam,GAM2);
Dvec2 = subs(D,gam,GAM2);
Tvec2 = subs(T,gam,GAM2);
CLvec2 = subs(CL,gam,GAM2);

PlotForces(GAM2,Lvec2,Dvec2,Tvec2,CLvec2,M*G)

%% Tram de descent
dtpuj = 0.20;
syms t cdo m v(t) rho s g thr v1
eq1 = thr - 0.5 * rho * s * v^2 * cdo + m * g == m*diff(v,t);
cond1 = v(0) == v1;
vt = dsolve(eq1,cond1);
xt = int(vt,t,[0 t]);
S = 11.25;
M = 850;
G = 9.81;
CD0 = 0.02;
RHO = 1.225;
V1 =  94.16; 
THR = 235000/V1;
T = linspace(0,dtpuj,1000);
symbs = [cdo m rho s g thr v1];
vals = [ CD0 M RHO S G THR V1];
VT = subs(vt,symbs,vals);
VTvec = subs(VT,t,T);

XT = subs(xt,symbs,vals);
XTvec = subs(XT,t,T);

figure
plot(T,VTvec,'linewidth',1.5)
xlabel("Time (s)",'interpreter','latex')
ylabel("Velocity (m/s)",'interpreter','latex')
title("Velocity vs time",'interpreter','latex')
figure
plot(T,XTvec,'linewidth',1.5)
xlabel("Time (s)",'interpreter','latex')
ylabel("Distance (m)",'interpreter','latex')
title("Distance covered",'interpreter','latex')

%% 4t quart tram de loop
syms clmax k cdo gam m v rho s g

qinf = 0.5 * v^2 * rho;
qinfs = qinf*s;
r = v^2/(qinfs*clmax/m -g);

gampunt = v/r;

cl = m * (g * cos(gam) + v^2/r)/qinfs;
cd = cdo + k * cl ^2;
l = qinfs * cl;
d = qinfs * cd;
t = d + m * g * sin(gam);

CLMAX = 1.774;
S = 11.25;
M = 850;
G = 9.81;
CD0 = 0.02;
K = 0.1;
RHO = 1.225;
V = 345/3.6;

symbs = [clmax k cdo m v rho s g];
vals = [CLMAX K CD0 M V RHO S G];
L = subs(l,symbs,vals);
D = subs(d,symbs,vals);
T = subs(t,symbs,vals);
CL = subs(cl,symbs,vals);

GAM3 = linspace(3*pi/2,2*pi,100);

Lvec3 = subs(L,gam,GAM3);
Dvec3 = subs(D,gam,GAM3);
Tvec3 = subs(T,gam,GAM3);
CLvec3 = subs(CL,gam,GAM3);

PlotForces(GAM3,Lvec3,Dvec3,Tvec3,CLvec3,M*G)
%% Joined
gamfin = cat(2,GAM1,GAM2,GAM3);
lfin = cat(2,Lvec1,Lvec2,Lvec3);
dfin = cat(2,Dvec1,Dvec2,Dvec3);
tfin = cat(2,Tvec1,Tvec2,Tvec3);
CLfin = cat(2,CLvec1,CLvec2,CLvec3);

PlotForces(gamfin,lfin,dfin,tfin,CLfin,M*G)
